package fr.univlille.iutinfo.r304.thermo.part1.model;

import fr.univlille.iutinfo.r304.thermo.part1.view.TextView;
import fr.univlille.iutinfo.r304.utils.Subject;

public class Thermogeekostat  extends Subject implements  ITemperature {

	private double temperature;
	@Override
	public void setTemperature(double d) {
		this.temperature=d;
		notifyObservers();

		
	}

	@Override
	public Double getTemperature() {
		return this.temperature;
	}

	@Override
	public void incrementTemperature() {
		
		this.temperature++;
		notifyObservers(this.temperature);
		
	}

	@Override
	public void decrementTemperature() {
		this.temperature--;
		notifyObservers();
		
	}

	@Override
	public String toString() {
		return   temperature +"c";
	}

   
}
