package fr.univlille.iutinfo.r304.thermo;

import fr.univlille.iutinfo.r304.thermo.part1.model.Thermogeekostat;
import fr.univlille.iutinfo.r304.thermo.part1.view.TextView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Thermogeekostat model = new Thermogeekostat();

        // Créez une première vue texte
        TextView textView1 = new TextView(model);
        textView1.show();

        // Créez une deuxième vue texte
        TextView textView2 = new TextView(model);
        textView2.show();
	}

}
