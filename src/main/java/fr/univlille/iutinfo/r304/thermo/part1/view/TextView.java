package fr.univlille.iutinfo.r304.thermo.part1.view;

import fr.univlille.iutinfo.r304.thermo.part1.model.Thermogeekostat;
import fr.univlille.iutinfo.r304.utils.Observer;
import fr.univlille.iutinfo.r304.utils.Subject;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;



public class TextView extends Stage implements ITemperatureView ,Observer {

	private Thermogeekostat model;
    private Label temperatureLabel = new Label(" 0");
	public TextView(Thermogeekostat model) {
		this.model=model;
		this.model.attach(this);
		Button incrementButton = new Button("+");
        incrementButton.setOnAction(event -> incrementAction());

        Button decrementButton = new Button("-");
        decrementButton.setOnAction(event -> decrementAction());

        HBox layout = new HBox(decrementButton, temperatureLabel, incrementButton);

        Scene scene = new Scene(layout, 200, 150);
        setScene(scene);

        setTitle("Thermogeekostat");
		

		
    }

	@Override
	public double getDisplayedValue() {
		return model.getTemperature();
	}

	@Override
	public void incrementAction() {
		model.incrementTemperature();
		temperatureLabel.setText(String.valueOf(this.model.getTemperature()));
		
		
	}

	@Override
	public void decrementAction() {
		model.decrementTemperature();
		temperatureLabel.setText(String.valueOf(this.model.getTemperature()));
		
		
	}

	@Override
	public void update(Subject subj) {
		this.update(subj, this.temperatureLabel);
	}

	@Override
	public void update(Subject subj, Object data) {
		this.temperatureLabel.setText(""+(Double)data);
	}
	
	

}
